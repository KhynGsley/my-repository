<?php
/**
 * @author Paul Okeke
 * @company VASCON SOLUTION
 * User: paulex10
 * Date: 24/11/2015
 * Time: 11:30
 */


/**
 * @param $message
 * @return array
 */
function generate_response_error($message)
{
    $response = array("status"=>'failed','message'=>$message);
    return $response;
}

/**
 * @param $message
 * @return array
 */
function generate_response_success($message)
{
    $response = array('status'=>'success','message'=>$message);
    return $response;
}

/**
 * @param $date
 * @return string
 */
function get_case_date($date){
    $caseDate = new DateTime($date);
//    $today = new DateTime();
//    $interval = $caseDate->diff($today);
    return $caseDate->format('d-M-y');//$interval->format('%m month, %d days');
}

/**
 * @param $num
 * @param $id
 * @return string
 */
function get_case_number($num, $id){
    $id['priority']['value'] = get_case_priority($id['priority']['value']);
    $id['cstm_complaint_category_c']['value'] = get_category_values($id['cstm_complaint_category_c']['value']);
    $id['cstm_complaint_sub_category_c']['value'] = get_sub_cat_values($id['cstm_complaint_sub_category_c']['value']);
    return "<span  data-case-id='".json_encode($id)."' class='ie-case-no'><a href='#'>#".$num."</a></span>";
}

function get_case_status($status){
    $status = strtolower($status);
    switch($status){
        case 'new':
            return '<span class="case-new ie-case-status">'.strtoupper($status).'</span>';
            break;
        case 'open':
            return '<span class="case-open ie-case-status">'.strtoupper($status).'</span>';
            break;
        case 'closed':
            return '<span class="case-closed ie-case-status">'.strtoupper($status).'</span>';
            break;
        case 'pending':
            return '<span class="case-pending ie-case-status">'.strtoupper($status).'</span>';
            break;
        case 'in_progress':
            return '<span class="case-progress ie-case-status">'.strtoupper('in progress').'</span>';
            break;
    }
    return '<span class="ie-case-status">'.$status.'</span>';
}

/**
 * @param $priority
 * @return string
 */
function get_case_priority($priority){
    $priority = strtolower($priority);
    switch($priority){
        case 'p1':
            return 'High';
            break;
        case 'p2':
            return 'Medium';
            break;
        case 'p3':
            return 'Low';
            break;
    }
    return 'undefined';
}

function get_major_category($v){
    $maj = array(

    );
}

function get_sub_category(){

}

function get_cases_columns(){
    return array(
        'id',
        'case_number',
        'name',
        'cstm_complaint_category_c',
        'cstm_complaint_sub_category_c',
        'priority',
        'status',
        'date_entered',
        'description',
        'cstm_resolution_summary_c'
    );
}

function get_category_values($cat){
    $category = array(
        'technical'=>'Technical',
        'ppm'=>'Pre-Paid Meter',
        'commecial'=>'Commercial',
        'requestenquiry'=>'Request/Enquiry',
        'payment'=>'Payment',
        'ami'=>'Smart Metering',
    );
    if(!isset($category[$cat])){
        return "undefined";
    }
    return $category[$cat];
}

function get_sub_cat_values($sub){
    $val = '{"faultytransformer":"Complaint on transformer","burningtransformer":"Complaint on transformer",
    "overloadedtransformer":"Complaint on transformer","vandalizedtransformer":"Complaint on transformer",
    "fluctuatingpowersupply":"Voltage related complaint","highvoltage":"Voltage related complaint",
    "lowvoltage":"Voltage related complaint","lossofphase":"No Power Supply","nopowersupply":"No Power Supply",
    "wirecut":"Wire cut","brokenpole":"Broken Pole","billclarification":"Bill Complaints",
    "billedamount":"Bill Complaints","continuousbilling":"Bill Complaints","estimatedbilling":"Bill Complaints",
    "highbillsoncodedacct":"Bill Complaints","requestforadjustment":"Bill Complaints",
    "overreading":"Bill Complaints","billsnotreceived":"Bill Complaints","decoding":"Bill Complaints",
    "highbills":"Bill Complaints","wrongreadingpicked":"Bill Complaints","migration":"Bill Termination",
    "stopbilling":"Bill Termination","suspendbilling":"Bill Termination","changeoftarrif":"Tariff",
    "billingonwrongtarrif":"Tariff","changeofname":"Wrong billing information",
    "wrongbillinginfo":"Wrong billing information","wrongopeningbal":"Wrong billing information",
    "statementofaccount":"Statement of account","burntmeter":"Complaint meter","damagedmeter":"Complaint meter",
    "blankmonitor":"Complaint meter","meternotdisplayingunit":"Complaint meter",
    "disconnectionerror":"Meter error","errore5":"Meter error","errore9":"Meter error",
    "damagedcard":"Complaint on meter card","lossofsmartcard":"Complaint on meter card",
    "unabletoloadtoken":"Inability to load meter","unabletodispenseenergy":"Inability to load meter",
    "unabletorechargeppm":"Inability to load meter","uncreditedunit":"Inability to load meter",
    "reqfortoken":"Inability to load meter","changeofownership":"Change of meter details",
    "correctionofname":"Change of meter details","reschedulingofarreas":"Rescheduling of arreas",
    "reqforppm":"Request for PPM","invalidcard":"Card related complaints",
    "lossofrechargecard":"Card related complaints","overscratchedcard":"Card related complaints",
    "unabletoloadcard":"Card related complaints","usedcard":"Card related complaints",
    "failedtransaction":"Card related complaints","unabletopayinthebank":"Unable to make payments",
    "unabletopaythroughiewebsite":"Unable to make payments",
    "unabletopaythroughinternetbanking":"Unable to make payments",
    "unabletopurchaseenergyviapos":"Unable to make payments","paymentplatformdowntime":"Unable to make payments",
    "deductiononpaymentmade":"Uncredited Payment","uncreditedpayment2":"Uncredited Payment",
    "mismatchofcustomerdetails":"Wrong details on receipt","paymentintowrongaccount":"Complaint on electric account",
    "faulty_smart_meter":"Meter Complaints","burnt_meter":"Meter Complaints",
    "meter_tampering":"Meter Complaints","meter_alarm_on":"Meter Complaints",
    "faulty_installation":"Meter Complaints","unabletoloadtoken2":"Unableto recharge meter",
    "unable_to_vend":"Unableto recharge meter","faulty_uiu":"User Interface Unit(UIU)",
    "loss_of_uiu":"User Interface Unit(UIU)","no_power_supply":"No Power Supply","loss_of_phase":"No Power Supply",
    "meter_bypass":"Meter Bypass","aboutie":"General Enquiries",
    "informationonofficeaddress":"General Enquiries","equiryaboutotherdisco":"General Enquiries",
    "enquiryonlistofbanks":"Enquiries on payments","enquiryonpaymentchannels":"Enquiries on payments",
    "enquiryontarrif":"Enquiry on tarrif","complimentarycalls":"Complimentary"}';

    $subs = json_decode($val, true);
    if(!isset($subs[$sub])){
        return 'undefined';
    }
    return $subs[$sub];
}

function CRM(){
    return array(
      'accounts'=>array(
          'fName'=>'cstm_first_name_c',
          'lName'=>'cstm_last_name_c',
      )
    );
}

function generate_email_url_tokens(){
    return substr(md5(rand(999, 99999999)), 0 , 20);
}

//one day is the max
function has_email_token_expired($createdTime){
        $format = "Y-m-d H:i:s";
        $date = \DateTime::createFromFormat($format, $createdTime);
        $currentTime = \DateTime::createFromFormat($format, date("Y-m-d H:i:s"));
        $elapsedTime = $currentTime->getTimestamp() - $date->getTimestamp();
        return $elapsedTime > 86400000;
}

define('acctFName', 'cstm_first_name_c');