<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('CRM::min.contact_us_min');
//    return view('CRM::contact_us');
});

$app->get('/portal', function() use ($app) {
//    if(!session()->has('is_logged_in') && !session('is_logged_in')){
//        return redirect('/');
//    }
    return view('CRM::min.portal_min');
});

$app->get('/terms_conditions', function() use ($app) {
    return view('CRM::ie_terms');
});
//
//$app->get('/email', function() use ($app) {
//    return view('CRM::template.email.user_created');
//});

