<?php

/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 09/12/2015
 * Time: 12:38
 */
namespace App\Modules\Authentication\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthValidator
{

    function validateAcctConfirmation(Request $request){
        return Validator::make($request->all(),
            array(
                'account_number'=>'required',
                'email'=>'required',
                'phone_number'=>'required|min:9|max:12'
            ),
            array(
                'account_number.required'=>"Your Account Number is required",
                'email.required'=>"Your Email is required",
                'phone_number.required'=>"Your Phone Number is required",
            )
        );
    }

    function validatePortalUserCreation(Request $request){
        return Validator::make($request->all(),
            array(
                'id'=>'required',
                'cstm_portal_username_c'=>'required',
                'cstm_portal_password_c'=>'required',
                'confirm_password'=>'required',
                'account_number'=>'required',
                'email1'=>'required'
            ),
            array(
                'cstm_portal_username_c.required'=>"Username is required",
                'cstm_portal_password_c.required'=>"Password is required",
                'confirm_password.required'=>"Confirm Password",
                'account_number.required'=>'The account number is required'
            )
        );
    }

    function validatePortalReset(Request $request){
        return Validator::make($request->all(),
            array(
                'zfc_mod_la_di'=>'required',
                'new_password'=>'required',
                'new_password_conf'=>'required'
            ),
            array(
                'zfc_mod_la_di.required'=>'What are you trying to do?',
                'new_password.required'=>'The new password is required',
                'new_password_conf'=>'Please confirm your new password'
            )
            );
    }

    function validatePortalAccountCreation(Request $request){
        return Validator::make($request->all(),
            array(
                'name'=>'required',
                'cstm_firstname_c'=>'required',
                'cstm_lastname_c'=>'required'
            ),
            array(
                'name.required'=>'The name is required',
                'cstm_firstname_c.required'=>'the first name is required',
                'cstm_lastname_c.required'=>'The last name is required'
            )
        );
    }

    function validateDataMigrationUpdate(Request $request){
        return Validator::make($request->all(),
            array(
                'cstm_tariff_class_c'=>'required'
            ),
            array(
                'cstm_tariff_class_c.required'=>'The tariff class is required',
                'id.required'=>'The id is required'
            )
        );
    }


}