<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 07/12/2015
 * Time: 21:22
 */

namespace App\Modules\Authentication\Model;
use Illuminate\Database\Eloquent\Model;

class PortalUser extends Model
{

    protected $table = "portal_user";

    protected $fillable = array("pt_user_acct_no", 'pt_user_name', 'pt_user_pass', 'pt_user_email');

    protected $primaryKey = "pt_user_id";

}