<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 07/12/2015
 * Time: 18:58
 */

namespace App\Modules\Authentication\Model;

use Illuminate\Database\Eloquent\Model;
class UserToken extends Model
{

    protected $table = "user_tokens";
    protected $fillable = ['usr_tk_token','usr_tk_salt'];
    protected $primaryKey = 'usr_tk_id';
}