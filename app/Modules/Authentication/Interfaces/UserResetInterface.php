<?php
/**
 * Created by PhpStorm.
 * User: paulex
 * Date: 25/02/16
 * Time: 09:19
 */

namespace App\Modules\Authentication\Interfaces;


interface UserResetInterface
{
    function create(array $data);

    function findById($id);

    function findUserByToken($token);

    function findUserById($id);

    function delete($id);

}