<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 07/12/2015
 * Time: 21:20
 */

namespace App\Modules\Authentication\Interfaces;


use App\Modules\Authentication\Model\PortalUser;

interface PortalUserInterface
{
    public function create(array $data);

    public function findUserById($id);

    public function findUserByAccountNo($acctNo);

    public function findUserByEmail($emailAddress);

    public function findByUserEmailAndAcctNo($email, $acctNo);

    public function edit(PortalUser $portalUser, array $data);

    public function delete($id);
}