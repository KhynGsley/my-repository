<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 07/12/2015
 * Time: 18:53
 */

namespace App\Modules\Authentication\Interfaces;


interface UserTokenInterface
{

    public function create(array $data);

    public function findByToken($token);

    public function edit();

    public function delete($id);

}