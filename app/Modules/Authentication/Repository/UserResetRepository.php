<?php
/**
 * Created by PhpStorm.
 * User: paulex
 * Date: 25/02/16
 * Time: 09:20
 */

namespace App\Modules\Authentication\Repository;


use App\Modules\Authentication\Interfaces\UserResetInterface;
use App\Modules\Authentication\Model\UserResetPassword;

class UserResetRepository implements UserResetInterface
{

    private $model;

    function __construct(UserResetPassword $model)
    {
        $this->model = $model;
    }

    function create(array $data)
    {
        return $this->model->create($data);
    }


    function findById($id)
    {
       return $this->model->find($id);
    }

    function findUserByToken($token)
    {
       return  $this->model->where("upr_token", $token)->get();
    }


    function findUserById($id)
    {
        return $this->model->where("pt_user_id", $id)->get();
    }

    function delete($id)
    {
        $model = $this->model->find($id);
        if($model!==null){
            $model->delete();
        }
    }
}