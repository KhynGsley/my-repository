<?php
/** ------------------------------------------------------------
 *  @Author Okeke Paul
 *  @Date 24/11/2015, Time: 11:43
 *  -------------------------------------------------------------
 *  @Description: Implemented the workflow for registration and
 *                login and the functions within. The creation of
 *                CRM account to support Data Migration,
 *
 *  -------------------------------------------------------------
 *  @Copyright 2015, VASCON Solutions.
 *  -------------------------------------------------------------
 *  Collaborator :
 *  Description : ${Describe what you've done...}
 *  Date :
 * ------------------------------------------------------------ */

namespace App\Modules\Authentication\Http\Controllers;
use App\Modules\Authentication\Interfaces\PortalUserInterface;
use App\Modules\Authentication\Interfaces\UserActivationInterface;
use App\Modules\Authentication\Interfaces\UserResetInterface;
use App\Modules\Authentication\Interfaces\UserTokenInterface;
use App\Modules\Authentication\Validator\AuthValidator;
use Illuminate\Http\Request;
use Asakusuma\SugarWrapper\Rest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Lumen\Routing\Controller as BaseController;



class AuthenticationController extends BaseController
{

    private $userToken;
    private $portalUser;
    private $fValidator;
    private $crm;
    private $activator;
    private $resetManager;

    public function __construct(UserTokenInterface $userToken, AuthValidator $authValidator,
                                PortalUserInterface $portalUser, UserActivationInterface $activator,
                                UserResetInterface $resetManager){
        $this->userToken = $userToken;
        $this->portalUser = $portalUser;
        $this->fValidator = $authValidator;
        $this->activator = $activator;
        $this->resetManager = $resetManager;
        $this->crm = new Rest();
        if(!$this->crm->is_logged_in()) {
            $this->crm->setUrl('http://199.189.84.76/power/service/v4/rest.php');
            $this->crm->setUsername('paulex');
            $this->crm->setPassword('Nigeriasns$1');
            $this->crm->connect();
        }
    }


    public function doLogin(Request $request){
        $portal_user = $request->get('portal_username');
        $portal_password = $request->get('portal_password');
        $fields = array();
        $options = array(
            'where'=>'accounts_cstm.cstm_portal_username_c="'.$portal_user.'"',
        );
        $result = $this->crm->get('Accounts', $fields, $options);
        if(sizeof($result)>0){
            $data = $result[0];
            $crmPassword = $data['cstm_portal_password_c'];
            if(!Hash::check($portal_password, $crmPassword)){
                $type = array('reason'=>"INVALID_LOGIN", "failure_message"=>"Invalid Username or Password");
                $response = generate_response_error($type);
                return response()->json($response);
            }
            $user = $this->portalUser->findUserByAccountNo($data['name']);
            if(!sizeof($user)>0){
                return response('Non-Authorized', 401)->header('WWW-Authenticate',
                    'basic="My Realm" location="http://localhost');
            }
            $user = $user[0];
            if($user->pt_user_activated==0) return response()
                ->json(generate_response_error(array('reason'=>"UN_ACTIVATED", "failure_message"=>"User not Activated")));

            $fileName = strlen($user->pt_user_image)>0 ? $user->pt_user_image : "default.jpg";
            $user->pt_user_image = route('portal.get.picture', ['p'=>$fileName]);
            $data['user'] = $user;
            $token = $this->createToken($portal_user, $portal_password);
            $data['token'] = $token;
            unset($data['cstm_portal_password_c']);
            $response = generate_response_success($data);
            return response()->json($response);
        }
        $type = array('reason'=>"INVALID_LOGIN", "failure_message"=>"Invalid Login");
        $response = generate_response_error($type);
        return response()->json($response);
    }


    public function createPassword(){

    }

    public function createToken($username, $password){
        $salt = md5($username.mt_rand(1, 564020));
        $userToken = Hash::make($username.$salt);
        $data = array(
            'usr_tk_salt'=>$salt,
            'usr_tk_token'=>$userToken
            );
        $result = $this->userToken->create($data);
        $result['usr_tk_salt'] = null;
        $result['id'] = null;
        if(is_null($result)){
            $response = generate_response_error("An Error Occurred");
            return response()->json($response);
        }
        session([
            'vas_token'=>$userToken,
            'is_logged_in'=>true,
        ]);
        return $result;
    }

    public function confirmAccount(Request $request){

        $v = $this->fValidator->validateAcctConfirmation($request);
        if($v->fails()){
            $response = generate_response_error($v->messages());
            return response()->json($response);
        }
        $accountNumber = $request->get('account_number');
        $phoneNo = $request->get('phone_number');
        $email = $request->get('email');
        $portalUser = $this->portalUser->findUserByAccountNo($accountNumber);
        $fields = array(
            'id',
            'name',
            'cstm_firstname_c',
            'cstm_lastname_c',
            'cstm_portal_username_c',
            'email1'
        );
        $options = array(
            'where'=>'accounts.name = "'.$accountNumber.'" or
            accounts.phone_alternate ="'.$phoneNo.'null'.'" or
            accounts.phone_office ="'.$phoneNo.'null'.'"'
        );
        $result = $this->crm->get('Accounts', $fields, $options);
        if(sizeof($result)<1){
            $type = array('reason'=>"NOT_FOUND");
            $response = generate_response_error($type);
            return response()->json($response);
        }

        $data = $result[0];
        if(strlen($data['cstm_portal_username_c'])>0 && sizeof($portalUser)>0){
            $type = array('reason'=>"PORTAL_USER_EXIST");
            $response = generate_response_error($type);
            return response()->json($response);
        }

        $response = generate_response_success($data);
        return response()->json($response);
    }

//    //Used only for Data Migration... hence this shouldn't be used on live production
//    function createCRMAccount(Request $request){
//        $v = $this->fValidator->validatePortalAccountCreation($request);
//        if($v->fails()){
//            $response = generate_response_error($v->messages());
//            return response()->json($response);
//        }
//        $data = $request->all();
//        $result = $this->crm->set("Accounts", $data);
//        if(!isset($result['id'])){
//            $response = generate_response_error($result);
//            return response()->json($response);
//        }
//        $response = generate_response_success($result);
//        return response()->json($response);
//    }
//
//    function migrateCRMData(Request $request, $userID){
//        $v = $this->fValidator->validateDataMigrationUpdate($request);
//        if($v->fails()){
//            $response = generate_response_error($v->messages());
//            return response()->json($response);
//        }
//        if(!$this->crm->is_valid_id($userID)){
//            $response = generate_response_error("Invalid Account ID");
//            return response()->json($response);
//        }
//        $data = array_merge(['id'=>$userID],$request->all());
//        $result = $this->crm->set("Accounts", $data);
//        if(!isset($result['id'])){
//            $response = generate_response_error($result);
//            return response()->json($response);
//        }
//        $response = generate_response_success($result);
//        return response()->json($response);
//    }

    function registerUser(Request $request){
        $v = $this->fValidator->validatePortalUserCreation($request);
        $myPortalUser = null;
        if($v->fails()){
            $response = generate_response_error($v->messages());
            return response()->json($response);
        }
        if(!$this->crm->is_valid_id($request->get('id'))){
            $response = generate_response_error("Invalid Account ID");
            return response()->json($response);
        }
        $data = $request->all();
        $data['cstm_portal_password_c'] = Hash::make($data['cstm_portal_password_c']);
        $result = $this->crm->set("Accounts", $data);
        if(!isset($result['id'])){
            $response = generate_response_error($result);
            return response()->json($response);
        }
        try{
            $data = array_merge($data, $this->doLocalCreation($data));
        }catch(\Exception $x){
            $data['cstm_portal_username_c'] = "";
            $data['cstm_portal_password_c'] = "";
            $this->crm->set("Accounts", $data);
            $type = array('reason'=>"DB_FAILURE", "failure_message"=>$x->getMessage());
            $response = generate_response_error($type);
            return response()->json($response);
        }

        Mail::send('CRM::template.email.user_activation_email', $data, function($msg) use ($data){
            $msg->to($data['email1'], $data['cstm_portal_username_c'])->subject('Welcome to IE Customer Portal');
        });
        $response = generate_response_success($result);
        return response()->json($response);
    }

    function doLocalCreation($data){
        $myPortalUser = $this->portalUser->create($data);
        $data['pt_user_email'] = $myPortalUser->pt_user_email;
        $data['pt_user_name'] = $myPortalUser->pt_user_name;
        $genTk = generate_email_url_tokens();
        $userActivator = $this->activator->create(
            array(
                "uac_token"=>Hash::make($genTk),
                "pt_user_id"=>$myPortalUser->pt_user_id
            )
        );
        $data['activation_link'] = route('user.activate.account',
            ['cnf'=>generate_email_url_tokens(),'uac_id'=>$userActivator['uac_id'], 'token'=>$genTk]);

        return $data;
    }

    function doLogout(Request $request){
        $token = $request->header('vas_token');
        $usrTk = $this->userToken->findByToken($token);
        if($usrTk!==null){
            $this->userToken->delete($usrTk->usr_tk_id);
        }
        $request->session()->flush();
        $response = generate_response_success('logout');
        return response()->json($response);
    }

    function activatePortalAccount($cnf, $uac_id, $token){
        $userActivator = $this->activator->findById($uac_id);
        if($userActivator==null){
            return view("CRM::template.failures.activation_failed", ['FAILURE_TYPE'=>"BROKEN_LINK"]);
        }
        if(!Hash::check($token,  $userActivator->uac_token)){
            return view("CRM::template.failures.activation_failed", ['FAILURE_TYPE'=>"FALSE IDENTITY"]);
        }
        $portalUser = $this->portalUser->findUserById($userActivator->pt_user_id);
        if(!sizeof($portalUser)>0){
            $this->activator->delete($uac_id);
            return view("CRM::template.failures.activation_failed", ['FAILURE_TYPE'=>"USER_NOT_FOUND"]);
        }
        //calculate the time elapsed for the activation link, the max is 1day
        if(has_email_token_expired($userActivator->created_at)){
            $this->activator->delete($uac_id);
            return view("CRM::template.failures.activation_failed", ['FAILURE_TYPE'=>"BROKEN_LINK"]);
        }
        $portalUser = $portalUser[0];
        $portalUser->pt_user_activated = 1;
        $portalUser->save();

        $data = array('pt_user_email'=>$portalUser->pt_user_email, 'pt_user_name'=>$portalUser->pt_user_name,);

        $this->activator->delete($uac_id);
        Mail::send('CRM::template.email.user_created', $data, function($msg) use ($data){
            $msg->to($data['pt_user_email'], $data['pt_user_name'])->subject('Welcome to IE Customer Portal');
        });
        session(['activated'=>true]);
        return redirect('reg/account/user/activated');//tell the user that the activation process was successful
    }

    function sendPasswordReset(Request $request){
        $email = $request->get('user_email');
        $accountNumber = $request->get('account_number');
        if(!isset($email) || !isset($accountNumber)) return response()
            ->json(generate_response_error("email and account number are both required"));

        //now we need to find if the user with the email exist
        $myPortalUser = $this->portalUser->findByUserEmailAndAcctNo($email, $accountNumber);
        if(!sizeof($myPortalUser)>0)
            return response()->json(generate_response_error(
                array('reason'=>"INVALID_USER", "failure_message"=>"The User with the email doesn't exist")));

        $myPortalUser = $myPortalUser[0];
        $data['pt_user_email'] = $myPortalUser->pt_user_email;
        $data['pt_user_name'] = $myPortalUser->pt_user_name;
        $data['pt_user_acct_no'] = $myPortalUser->pt_user_acct_no;
        //we need to generate a link for password reset.. and also add
        //the reset state to the db to verify later when the reset link is clicked...
        //the reset should be done only once
        $gtk = generate_email_url_tokens();
        $urp = $this->resetManager->create(
            array('urp_token'=>Hash::make($gtk), 'pt_user_id'=>$myPortalUser->pt_user_id));

        $data['reset_link'] = route('user.generate.reset',
            ['cnf'=>generate_email_url_tokens(), 'urp_id'=>$urp->urp_id, 'token'=>$gtk]);

        Mail::send('CRM::template.email.user_reset_password_email', $data, function($msg) use ($data){
            $msg->to($data['pt_user_email'], $data['pt_user_name'])->subject('IE Customer Portal - Reset Password');
        });

        $request = generate_response_success($data);
        return response()->json($request);
    }

    function generateResetLink($cnf, $urp_id, $token){
        $resetPassword = $this->resetManager->findById($urp_id);
        if($resetPassword==null){
            return view("CRM::template.failures.activation_failed", ['FAILURE_TYPE'=>"BROKEN_LINK"]);
        }
        if(!Hash::check($token,  $resetPassword->urp_token)){
            return view("CRM::template.failures.activation_failed", ['FAILURE_TYPE'=>"FALSE IDENTITY"]);
        }
        $portalUser = $this->portalUser->findUserById($resetPassword->pt_user_id);
        if(!sizeof($portalUser)>0){
            $this->resetManager->delete($urp_id);
            return view("CRM::template.failures.activation_failed", ['FAILURE_TYPE'=>"USER_NOT_FOUND"]);
        }

        //calculate the time elapsed for the activation link, the max is 1day
        if(has_email_token_expired($resetPassword->created_at)){
            $this->resetManager->delete($urp_id);
            return view("CRM::template.failures.activation_failed", ['FAILURE_TYPE'=>"BROKEN_LINK"]);
        }
        $portalUser = $portalUser[0];

        $data = array(
            'user_id'=>$portalUser->pt_user_id,
            'username'=>$portalUser->pt_user_name,
            'form_route'=>route('user.reset.account')
        );

        return view('CRM::template.system.forgot_password', $data);
    }

    function  resetPortalUserAccount(Request $request){
        $v = $this->fValidator->validatePortalReset($request);
        if($v->fails()){
            return view('CRM::template.system.forgot_password')->with($v->messages());
        }
        $pass = $request->get('new_password');
        if($pass!==$request->get('new_password_conf')){
            return redirect()->back()->withInput(array('error'=>"The password doesn't match"));
        }

        $userId = $request->get('zfc_mod_la_di');
        $myPortalUser = $this->portalUser->findUserById($userId);
        if(!sizeof($myPortalUser)>0){
            return redirect()->back()->withInput(array('error'=>"Error User doesn't exist"));
        }
        $myPortalUser = $myPortalUser[0];
        $data = array();
        $data['cstm_portal_password_c'] = Hash::make($pass);
        $fields = array('id');
        $options = array('where'=>'accounts.name = "'.$myPortalUser->pt_user_acct_no.'"');

        $result = $this->crm->get('Accounts', $fields, $options);
        if(sizeof($result)<1){
            return redirect()->back()->withInput(array('error'=>"Account doesn't exist"));
        }
        $data['id'] = $result[0]['id'];
        $result = $this->crm->set("Accounts", $data);

        if(!isset($result['id'])){
            $response = generate_response_error($result);
            return response()->json($response);
        }
        $myPortalUser->pt_user_pass = $data['cstm_portal_password_c'];
        $myPortalUser->save();

        //now we now finally delete this reset state from DB
        $m = $this->resetManager->findUserById($myPortalUser->pt_user_id);
        if(sizeof($m)>0)  $this->resetManager->delete($m[0]['urp_id']);

        $data = array(
            'user_id'=>$myPortalUser->pt_user_id,
            'username'=>$myPortalUser->pt_user_name,
            'form_route'=>route('user.reset.account'),
            'has_reset'=>true
        );
        return view('CRM::template.success.password_reset_success', $data);
    }
}