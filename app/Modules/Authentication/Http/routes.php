<?php
//$app->group(['middleware'=>'authentication'],function($app){
//
//
//
//});

$app->post('/reg/login',
    [
        'as'=>'portal.login',
        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@doLogin'
    ]);

$app->post('/reg/logout',
    [
        'as'=>'portal.logout',
        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@doLogout'
    ]);

$app->post('/reg/confirm',
    [
        'as'=>'portal.reg.confirm',
        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@confirmAccount'
    ]);

//$app->post('/reg/create/account',
//    [
//        'as'=>'portal.reg.create.account',
//        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@createCRMAccount'
//    ]);

$app->post('/reg/create/user',
    [
        'as'=>'portal.reg.create',
        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@registerUser'
    ]);

$app->get('reg/account/{cnf}/{uac_id}/activate/{token}',
    [
        "as"=>"user.activate.account",
        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@activatePortalAccount'
    ]);

$app->post('reg/account/password/reset',
    [
        'as'=>'user.send.reset',
        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@sendPasswordReset'
    ]);

$app->get('reg/account/{cnf}/{urp_id}/reset/{token}',
    [
        "as"=>"user.generate.reset",
        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@generateResetLink'
    ]);

$app->post('reg/account/reset/account',
    [
        "as"=>"user.reset.account",
        'uses'=>'App\Modules\Authentication\Http\Controllers\AuthenticationController@resetPortalUserAccount'
    ]);

$app->get('reg/account/user/activated', function(){
        if(\Illuminate\Support\Facades\Session::has('activated')){
            \Illuminate\Support\Facades\Session::forget('activated');
            return view('CRM::template.success.activation_success');
        }
    return redirect('/');
});
