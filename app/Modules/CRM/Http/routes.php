<?php
$app->group(['middleware'=>'authentication'],function($app){

    $app->get('/contact/cases/{id}',
        [
            'as'=>'contact.cases',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@getContactCases'
        ]);

    $app->get('/contact/recent/cases/{id}',
        [
            'as'=>'contact.recent.cases',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@getContactRecentCases'
        ]);


    $app->get('/user/details/{id}',
        [
            'as'=>'user.details',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@getUserDetails'
        ]);

    $app->post('/user/details/edit/{id}',
        [
            'as'=>'user.details.edit',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@editUserDetails'
        ]);



    $app->get('/case/notes/{id}',
        [
            'as'=>'case.notes',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@getCaseNotesAndAttachment'
        ]);

    $app->get('/case/get/{id}',
        [
            'as'=>'case.get',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@getSingleCase'
        ]);

    $app->post('/case/add',
        [
            'as'=>'case.add',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@addCase'
        ]);

    $app->get('/case/edit/{id}',
        [
            'as'=>'case.edit',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@editCase'
        ]);

    $app->post('/case/note/add',
        [
            'as'=>'case.note.add',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@addNoteAndAttachment'
        ]);

    $app->post('portal/change/picture',
        [
            'as'=>'portal.change.picture',
            'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@changeProfilePicture'
        ]
    );
    
});

$app->get('case/download/attachment/{nid}',
    [
        'as'=>'case.download.attachment',
        'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@downloadAttachment'
    ]
);


$app->get('portal/get/picture/{p}', [
    'as'=>'portal.get.picture',
    'uses'=>'App\Modules\CRM\Http\Controllers\CRMController@getProfilePicture'
]);

