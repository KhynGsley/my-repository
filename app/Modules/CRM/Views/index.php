<!--/**
* @author Okeke Paul Ugochukwu
* @company VASCON Solutions
* @company VAS-CONSULTING
* @email pugochukwu@vas-consulting.com
* @alt-email donpaul120@gmail.com
* Date: 08/12/2015
* Time: 12:15
*/-->
<!DOCTYPE html>
<html ng-app="application">
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:100,200,400,300,600,700&subset=all"
          rel="stylesheet" type="text/css"/>
    <link href="../public/build/css/api.portal.css" rel="stylesheet"/>
    <link href="../public/assets/images/ie_logo.png" rel="shortcut icon" type="image/x-icon" />

    <script src="../public/build/js/ie.lib.main.js"></script>
    <script src="../public/build/js/ie.portal.min.js"></script>

    <title>IE Customer Portal</title>
</head>

<body>
    <div class="main">
        <div class="ie-overlay"></div>
        <div class="ie-portal-design">
           <div class="ie-portal-actions">
               <button id="ie-reg-btn">REGISTER</button>
               <button id="ie-login-btn">LOGIN</button>
               <button id="ie-faqs-btn">FAQs</button>
           </div>

            <div class="ie-major-content">
                <div>
                    <img src="../public/assets/images/ie_logo.png">
                </div>
                <div>IE Customer Portal</div>
                <span class="ie-slogan">
                    Bringing energy to life...
                </span>
            </div>
            <div class="ie-footer">

                <div class="ie-footer-item">
                    <img width="30" height="35" src="../public/assets/images/ie_logo.png">
                    <span class="ie-footer-item-name" style="font-weight: 600;margin-top: 4px">
                        Ikeja Electric
                    </span>
                </div>

                <div class="ie-footer-right">
                    <div class="ie-footer-item">
                        <img width="30" height="30" src="../public/assets/images/phone_icon.png">
                    <span class="ie-footer-item-name">
                        01-7000-250,
                        01-448-3900,
                        0700-022-5543
                    </span>
                    </div>

                    <div class="ie-footer-item">
                        <img width="30" height="30" src="../public/assets/images/email_icon.png">
                        <span class="ie-footer-item-name">customercare@ikejaelectric.com</span>
                    </div>
                </div>

            </div>
        </div>

        <div class="ie-side-nav">
            <div class="ie-side-header">
                <img class="ie-side-back-btn" width="20" height="20" src="../public/assets/images/down_arrow_white.png" alt="back"/>
                <span id="ie-side-name"></span>
            </div>
            <div class="ie-reg-page" ng-controller="RegController as regCtrl">
                <div class="ie-steps-count">
                    <span ng-show="regCtrl.step1">Step 1 of 2</span>
                    <span  ng-show="regCtrl.step2">Step 2 of 2</span>
                </div>
                <div ng-show="regCtrl.step1 || regCtrl.onReg" class="ie-side-content-title">{{regCtrl.pageState}}</div>
                <div ng-show="regCtrl.onError && regCtrl.step1 && !regCtrl.onReg"
                     class="ie-side-content-error">{{regCtrl.regErrorMsg}}</div>
                <div class="ie-side-content">
                    <div ng-show="regCtrl.onReg" class="ie-on-login">
                        <img src="../public/assets/images/login_in_gif.gif">
                    </div>

                    <!--Start of Account Details..-->
                    <div ng-show="regCtrl.step2 && !regCtrl.onReg" class="ie-step-2-screen">

                        <div class="ie-account-details">
                            <span class="ie-hi">Hi!</span>
                            <div class="ie-account-full-name">
                                {{regCtrl.accountFullName}}
                            </div>
                            <span class="ie-account-email">{{regCtrl.accountEmail}}</span>
                        </div>

                        <form name="create" ng-submit="regCtrl.register()">
                            <!---->
                            <input type="text" name="username" placeholder="Enter Username"
                                   ng-model="regCtrl.createForm.cstm_portal_username_c" required/>

                            <input type="email" name="email" placeholder="Enter E-mail:"
                                   ng-model="regCtrl.createForm.email1" required/>

                            <input type="password" name="password" placeholder="Enter Password:"
                                   ng-model="regCtrl.createForm.cstm_portal_password_c" required/>

                            <span class="ie-form-error" ng-show="!create.p_password.$pristine &&
                                  regCtrl.createForm.confirm_password!==regCtrl.createForm.cstm_portal_password_c">
                                Password doesn't match
                            </span>
                            <input type="password" name="p_password" placeholder="Confirm Password:"
                                   ng-model="regCtrl.createForm.confirm_password" required/>

                            <button type="submit" ng-disabled="create.$invalid ||
                            regCtrl.createForm.confirm_password!==regCtrl.createForm.cstm_portal_password_c"
                                    class="ie-login-btn">
                                <img width="17" height="17" src="../public/assets/images/login_icon.png">
                                Create
                            </button>

                        </form>
                        <div class="ie-reg-opt-out" ng-click="regCtrl.optOut()">
                            This account details doesn't look like mine
                        </div>
                    </div>
                    <!--End of Account Details...-->

                    <form ng-show="!regCtrl.onReg && regCtrl.step1" name="reg" ng-submit="regCtrl.confirm()">
                        <span class="ie-form-error"
                              ng-show="reg.accountNumber.$error.minlength && !reg.accountNumber.$pristine">
                                Incomplete account number.
                        </span>
                        <input type="tel" name="accountNumber" autocomplete="off"
                               class="" placeholder="Account Number" ng-minlength="9"
                               maxlength="18"
                               ng-model="regCtrl.confirmForm.account_number" required/>

                        <input type="email" name="email" class=""
                               placeholder="email@email.com" autocomplete="off"
                               ng-model="regCtrl.confirmForm.email" required/>

                        <span class="ie-form-error"
                              ng-show="reg.phoneNumber.$error.minlength && !reg.phoneNumber.$pristine">
                                Invalid mobile number.
                        </span>
                        <input type="tel" name="phoneNumber" contenteditable="true"
                               placeholder="Phone No." autocomplete="off" ng-minlength="9" maxlength="11"
                               ng-model="regCtrl.confirmForm.phone_number" required/>

                        <button type="submit" ng-disabled="reg.$invalid" class="ie-login-btn">
                            <img width="17" height="17" src="../public/assets/images/login_icon.png">
                            Create
                        </button>
                        <button type="reset" class="ie-reset-btn">
                            Reset
                        </button>
                    </form>

                    <div ng-show="regCtrl.regSuccess" class="ie-reg-success">
                        <div><img width="150"  height="150" src="../public/assets/images/thumbs_up_icon.jpeg"></div>
                        <div class="ie-reg-success-message">Registration Successful</div>
                        <div class="ie-proceed-to-login">
                            <button id="ieLoginProceed">Proceed to login</button>
                        </div>
                    </div>

                </div>
                <div class="ie-help ie-reg-help">
                    <img height="40" width="40" src="../public/assets/images/help_icon_2.png"/>
                    <span> Help</span>
                </div>
                <div class="ie-reg-help-content">
                    <div class="ie-help-header">
                        Registration Help
                        <span class="ie-help-closer">X</span>
                    </div>
                    <div class="ie-reg-help-content-text">
                        <ul>
                            <li>
                                To register, simply type in your
                                account number, a valid mobile number
                                related to the owner of the account and
                                a valid email address of the person
                                responsible to this account in the
                                respective fields above then click on save.
                            </li>

                            <li>
                                Once your details is been verified, you will be notified
                                of your portal login credentials
                                via the email address you provided.
                            </li>

                            <li>
                                The system might return an error message saying
                                <span style="font-weight: 600">
                                    Account Details Not Found
                                </span>
                                This means we currently haven't set up your account to access the portal.<br/>
                                To enable portal access please call the Ikeja Electric Customer care, supply them
                                your account details and try again.
                            </li>


                        </ul>
                    </div>
                </div>
            </div>

            <div class="ie-login-page" ng-controller="LoginController as loginCtrl">
                <div class="ie-side-content-title">{{loginCtrl.pageState}}</div>

                <div ng-show="loginCtrl.onError && !loginCtrl.onLogin"
                     class="ie-side-content-error">{{loginCtrl.loginErrorMsg}}</div>

                <div class="ie-side-content">
                    <div ng-show="loginCtrl.onLogin" class="ie-on-login">
                        <img src="../public/assets/images/login_in_gif.gif">
                    </div>
                    <form ng-show="!loginCtrl.onLogin" name="login" ng-submit="loginCtrl.login()">
                        <input type="text" name="username" class="" autocomplete="off"
                               placeholder="Username" ng-model="loginCtrl.loginForm.portal_username"
                               required/>

                        <input type="password" name="password" class="" autocomplete="off"
                               placeholder="Password" ng-model="loginCtrl.loginForm.portal_password"
                               required/>

                        <button class="ie-login-btn">
                            <img width="17" height="17" src="../public/assets/images/login_icon.png">
                            Login
                        </button>
                        <button type="reset" class="ie-reset-btn">
                            <!--                            <img width="17" height="17" src="../assets/images/login_icon.png">-->
                            Reset
                        </button>
                    </form>
                </div>

                <div class="ie-help ie-login-help">
                    <img height="40" width="40" src="../public/assets/images/help_icon_2.png"/>
                    <span> Help</span>
                </div>
                <div class="ie-login-help-content">
                    <div class="ie-help-header">
                        Login Help
                        <span class="ie-help-closer">X</span>
                    </div>
                    <div class="ie-login-help-content-text">
                        <ul>
                            <li>
                                To login simply type in your username and password
                                and click on login.
                            </li>

                            <li>
                                If you don't have a username and password
                                please kindly visit the registration section.
                            </li>
                            <li>
                                If you've registered and didn't receive your login credentials
                                please kindly call Ikeja Electric call center.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>
<script src="../public/build/js/utility/ie.portal.utility.js"></script>
</html>