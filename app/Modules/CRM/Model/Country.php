<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 24/10/2015
 * Time: 23:26
 */

namespace App\Modules\Healthbox\Model;


use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $fillable = ['country','iso2'];

    public function state(){
        return $this->hasMany('App\Modules\Healthbox\Model\State','country_id');
    }

}