<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 24/10/2015
 * Time: 23:31
 */

namespace App\Modules\Healthbox\Repository;


use App\Modules\Healthbox\Interfaces\CountryInterface;
use App\Modules\Healthbox\Model\Country;

class CountryRepository implements CountryInterface
{

    public function __construct(Country $country){
        $this->model = $country;
    }

    public function findAllCountries()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function edit(Country $country, array $data)
    {
        $country->fill($data);
        $country->save();
        return $country;
    }

    public function findById($id)
    {
        $country = $this->model->find($id);
        if($country!=null){
            return $country->first();
        }
        return null;
    }

    public function delete($id)
    {
        $country = $this->model->find($id);
        if($country!==null){
             $country->delete();
            return 1;
        }
        return -1;
    }
}