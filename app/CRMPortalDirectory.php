<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 20/04/2015
 * Time: 23:45
 */
namespace App;


class CRMPortalDirectory {

    public static function getPath($path)
    {
        return array_diff(scandir($path), array('.','..'));
    }
}