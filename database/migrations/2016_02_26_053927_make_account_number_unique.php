<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAccountNumberUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal_user', function (Blueprint $table) {
            $table->unique('pt_user_acct_no');
            $table->unique('pt_user_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal_user', function (Blueprint $table) {
            $table->dropUnique('pt_user_acct_no');
            $table->dropUnique('pt_user_name');
        });
    }
}
