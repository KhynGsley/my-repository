<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class DataMigrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataDump = app('App\Modules\DataDump\Interfaces\IEDataDumpInterface');
        $dataCount = $dataDump->getDataCount();
        $batchCount = round($dataCount/400);
        $currentPos = $dataCount;
        $this->command->info($batchCount);
        $authCtrl = App::make('App\Modules\Authentication\Http\Controllers\AuthenticationController');

        while($currentPos>0){
            $skipCount = $dataCount - $currentPos;
            $records = $dataDump->findBySkippingAndLimiting($skipCount, $batchCount);
            foreach($records as $key=>$record){
                $accountNumber = str_replace('-', '', $record->account_no);
                $this->command->info($accountNumber);
                $postData = array(
                    'account_number'=>$accountNumber,
                    'phone_number'=>'08139000000',
                    'email'=>'okeketn@yahio.dom'
                );
                $request = Request::create('http://localhost/portalapi/public/reg/confirm', 'POST', $postData);
                $result = $authCtrl->confirmAccount($request);
                $response = json_decode($result->getContent(), true);
                if($response['status']=='failed'){
                    //it means this account doesn't exist so we create a  new one
                    $this->command->info("The Account Details doesn't exist on the CRM");
                    $request = Request::create('http://localhost/portalapi/public/reg/create/account', 'POST', $this->makePostData($record));
                    $this->command->info("Creating Account :".$accountNumber ." Name: ".$record->account_name);
                    $result = $authCtrl->createCRMAccount($request);
                    $this->command->info("Created Account :". $result->getContent());
                }else {
                    $this->command->info("The user already exist on the CRM");
                }
            }
            $currentPos = $currentPos - $batchCount;
            sleep(10);
        }
    }

    function makePostData($record){
        $accountNumber = str_replace('-', '', $record->account_no);
        $lastName = substr($record->account_name, 0, stripos($record->account_name, " "));
        $firstName = substr($record->account_name, stripos($record->account_name, " "), strlen($record->account_name));
        return array(
            'name'=>$accountNumber,
            'cstm_lastname_c'=>$lastName,
            'cstm_firstname_c'=>$firstName,
            'cstm_is_data_dump_c'=>1,
        );
    }
}
