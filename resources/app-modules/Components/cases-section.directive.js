/**
 * Created by paulex10 on 24/11/2015.
 */
angular.module('application')
    .directive('casesSection', function(){
        return {
            restrict: 'E',
            templateUrl :'../resources/app-modules/Components/cases-section.directive.html'
        }
    });