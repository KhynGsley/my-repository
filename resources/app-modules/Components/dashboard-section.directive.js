/**
 * Created by paulex10 on 02/12/2015.
 */
angular.module('application')
    .directive('dashboardSection', function(){
        return {
            restrict: 'E',
            templateUrl :'../resources/app-modules/Components/dashboard-section.directive.html'
        }
    });