/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 * with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 12/12/2015.
 */
angular.module('application')
    .controller('ErrorController', ['errorService',ErrorController]);

function ErrorController(errorService){
    this.errorSvc = errorService;
    this.hasError = errorService.hasError;
    this.errorMessage = errorService.errorMessage;
    this.errorResolution = errorService.errorResolution;
    this.hasRetry = errorService.hasRetry;
}