/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * @author Paul Okeke
 * Created by paulex10 on 24/11/2015.
 */
angular.module('application')
    .controller('ProfileController', ['$timeout','profileService','userService', ProfileController]);



function ProfileController($timeout,profileSvc, userService){
    this.profileSvc = profileSvc;
    this.timeout = $timeout;
    this.userImage = userService.getUserImage();
    setSectionPosition();
    this.user = {};
    this.editMode = false;
    this.accountNumber = null;
    this.actionLabel = "EDIT PROFILE";
    this.resume = false;
    this.pauseReason = "Initializing View...";
    this.userForm = {};
    this.onStart();
    userService.registerComp(this);
}


ProfileController.prototype.toggleProfileEdit = function(profile){
    this.editMode = !this.editMode;
    if(this.editMode){
        this.actionLabel = "SAVE PROFILE";
    }else{
        //user is saving a record.. trigger save mode
        this.onSave(profile);
        //angular.element('#submit').trigger('click');
        this.actionLabel = "EDIT PROFILE";
    }
};

ProfileController.prototype.onResume = function(){
    this.resume = true;
};

ProfileController.prototype.onStart = function(){
    var self = this;
    this.profileSvc.getUserDetails(function(result){
        //resume the application
        self.onResume();
        //create the view and model values
        self.onCreateView(result);
    });
};

ProfileController.prototype.onCreateView = function($data){
    if($data){
        this.accountNumber = $data['name'];
        this.userForm = {
            "cstm_lastname_c":$data['cstm_lastname_c'],
            "cstm_firstname_c":$data['cstm_firstname_c'],
            "other_name":$data['other_name'],
            "email1":$data['email1'],
            "phone_alternate":$data['phone_alternate'],
            "phone_office":$data['phone_office'],
            "billing_address_street":$data['billing_address_street'],
            "billing_address_city":$data['billing_address_city'],
            "billing_address_state":$data['billing_address_state'],
            "id":$data['id']
        };
    }
};

ProfileController.prototype.onSave = function(profile){
    var self = this;
    this.resume = false;
    this.pauseReason = "Saving Your Data...";
    this.profileSvc.editUserDetails(this.userForm.id, this.userForm, function(result){
            if(result){
                self.resume = true;
                self.pauseReason = "";
            }else{
                self.pauseReason = "Your data wasn't saved";
                self.timeout(function(){
                    self.resume = true;
                    self.pauseReason = "";
                },4000)
            }
    });
};
