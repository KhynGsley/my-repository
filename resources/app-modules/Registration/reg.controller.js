/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * @author Paul Okeke
 * @company VASCON SOLUTION
 * @copyright 2015
 * Created by paulex10 on 09/12/2015.
 */
angular.module('application')
    .controller('RegController', ['regService', RegController]);



function RegController(regService) {
    this.regService = regService;
    this.onReg = false;
    this.onError = false;
    this.step1 = true;
    this.step2 = false;
    this.regSuccess = false;
    this.accountFullName = "";
    this.accountEmail = "";
    this.pageState = "Create User";
    this.regErrorMsg = "";

    this.confirmForm = {
        account_number:null,
        phone_number:null,
        email:null,
        terms:null
    };
    this.createForm = {
        id:null,
        cstm_portal_username_c:null,
        cstm_portal_password_c:null,
        confirm_password:null,
        email1:null,
        account_number:null
    };
}

RegController.prototype.onRegSuccess = function(){
    this.pageState = "";
    this.onError = false;
    this.regSuccess = true;
    this.step2 = false;
    this.step1 = false;
};

RegController.prototype.register = function(){
    this.onReg = true;
    this.pageState = "Creating...";
    this.regService.registerUser(this.createForm, function(status, $data){
        this.onReg = false;
        console.log($data);
        if(status){
            this.onRegSuccess();
        }
    }.bind(this));
};

RegController.prototype.confirm = function(){
    this.onReg = true;
    this.pageState = "Processing...";
    this.regService.confirmUser(this.confirmForm, function(status, $data){
        this.onReg = false;
        this.pageState = "Create User";
        if(status){
            this.step1 = false;
            this.step2 = true;
            this.accountFullName = $data['cstm_firstname_c'] + " " + $data['cstm_lastname_c'];
            this.accountEmail = $data['email1'];
            this.createForm.id = $data['id'];
            this.createForm.email1 = $data['email1'];
            this.createForm.account_number = $data['name'];
            //console.log($data);
        }else{
            this.onError = true;
            switch ($data){
                case "NOT_FOUND":
                    this.regErrorMsg = "Account Details Not Found";
                    break;
                case "PORTAL_USER_EXIST":
                    this.regErrorMsg = "User Already Exist";
                    break;
            }
        }
    }.bind(this))
};

RegController.prototype.optOut = function(){
    this.step2 = false;
    this.step1 = true;
    this.accountFullName = null;
    this.accountEmail = null;
    var keys1 = Object.keys(this.createForm);
    for(var b=0;b<keys1.length;b++){
        if(this.createForm.hasOwnProperty(keys1[b]))
            this.createForm[keys1[b]] = null;
    }
    var keys = Object.keys(this.confirmForm);
    for(var i=0;i<keys.length;i++){
        if(this.confirmForm.hasOwnProperty(keys[i]))
            this.confirmForm[keys[i]] = null;
    }
};

