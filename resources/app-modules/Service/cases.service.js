/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 24/11/2015.
 */
angular.module('application')
    .service('caseService', ['$http','$timeout','$localStorage','errorService','taskManager', CaseService]);

function CaseService($http, $timeout, $localStorage, errorService, taskManager){
    this.http = $http;
    this.tOut = $timeout;
    this.sharedPref = $localStorage;
    this.$errorSvc = errorService;
    this.taskMgr = taskManager;
}

CaseService.prototype.getAccountRecentCases = function(userID, callback){
    var self = this;
    var taskId = this.taskMgr.createTask('Retrieving Your Recent Cases...');
    this.http.get(getPathUrl('/contact/recent/cases/'+userID, true, this.sharedPref))
        .then(
        function(response){
            if(response.data.status=="success"){
                callback(true, response.data.message);
            }else{
                callback(false, response.data.message.reason);
            }
            self.taskMgr.removeTask(taskId);
        },
        function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.taskMgr.removeTask(taskId);
                        self.getAccountRecentCases(userID, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    callback(false, '');
                    self.taskMgr.removeTask(taskId);
                }
            });
        });
};

CaseService.prototype.getCaseNotes = function($caseId, callback){
    var self = this;
    var taskId = this.taskMgr.createTask('Retrieving a case notes');
    this.http.get(getPathUrl('/case/notes/'+$caseId, true, this.sharedPref), {cache:false})
        .then(function(response){
            //console.log(response);
            if(response.data.status=="success"){
                callback(response.data.message);
            }
            self.taskMgr.removeTask(taskId);
        },
        function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.taskMgr.removeTask(taskId);
                        self.getCaseNotes($caseId, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    //callback(false);
                    //for case notes we need to continue reload
                    self.taskMgr.removeTask(taskId);
                    self.getCaseNotes($caseId, callback);
                }
            });
        })
};

CaseService.prototype.addNoteAttachment = function(data, callback){
    var self = this;
    var taskId = this.taskMgr.createTask('Adding a note to a case');
    self.http.post(getPathUrl('/case/note/add', true, this.sharedPref), data)
        .then(function(response){
            console.log(response);
            if(response.data.status==="success"){
                callback(response.data.message);
            }else{
                callback(false);
            }
            self.taskMgr.removeTask(taskId);
        },
        function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.taskMgr.removeTask(taskId);
                        self.addNoteAttachment(data, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    callback(false);
                    self.taskMgr.removeTask(taskId);
                }
            });
        });
};


CaseService.prototype.createCase = function (data, callback){
    var self = this;
    var taskId = this.taskMgr.createTask('Creating your complaint');
    self.http.post(getPathUrl('/case/add', true, this.sharedPref), data)
        .then(function(response){
            console.log(response);
            if(response.data.status==="success"){
                callback(response.data.message);
            }else{
                callback(false);
            }
            self.taskMgr.removeTask(taskId);
        },
        function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.taskMgr.removeTask(taskId);
                        self.createCase(data, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    callback(false);
                    self.taskMgr.removeTask(taskId);
                }
            });
        });
};

CaseService.prototype.editCase = function (data, caseID ,callback){
    var self = this;
    self.http.post(getPathUrl('/case/edit/'+caseID, true, this.sharedPref), data)
        .then(function(response){
            console.log(response);
            if(response.data.status==="success"){
                callback(response.data.message);
            }else{
                callback(false);
            }
        },
        function(error){
            //console.log(error);
            ////check the error type first
            //setTimeout(function(){
            //
            //},1000);
            self.$errorSvc.responseError(error);
        });
};


CaseService.prototype.getSingleCase = function(caseID, callback){
    var self = this;
    var taskId = this.taskMgr.createTask('Retrieving a single case');
    self.http.get(getPathUrl("/case/get/"+caseID, true, this.sharedPref))
        .then(function(response){
            console.log(response);
            if(response.data.status==="success"){
                callback(response.data.message);
            }else{
                callback(false);
            }
            self.taskMgr.removeTask(taskId);
        }, function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.taskMgr.removeTask(taskId);
                        self.getSingleCase(caseID, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    self.taskMgr.removeTask(taskId);
                    self.getSingleCase(caseID, callback);
                }
            });
        });
};