/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 24/11/2015.
 */
angular.module('application')
    .service('userService', ['$localStorage','$http','taskManager', UserService]);

function UserService($localStorage, $http, taskManager){
    this.userId = null;
    this.accountNumber = null;
    this.userFullName = null;
    this.userImage = null;
    this.http = $http;
    this.sharedPref = $localStorage;
    this.taskMgr = taskManager;
    this.components = [];
}
UserService.prototype.authCheck = function(){
    if(this.sharedPref.iPzmz===undefined || this.sharedPref.iPzmz===null){
        //TODO:: call a service that redirects the user
        window.location.href =  baseUrl();
        return false;
    }
    return true;
};

UserService.prototype.setUserId = function($userID){
    this.userId = $userID;
};

UserService.prototype.getUserId = function(){
    if(this.authCheck())
        return this.sharedPref.iPzmz.ieBid;
};

UserService.prototype.setAccountNumber = function(acctNo){
    this.accountNumber = acctNo;
};

UserService.prototype.getAccountNumber = function(){
    if(this.authCheck()) return this.sharedPref.iPzmz.ieAno;
};

UserService.prototype.getUserFullName = function(){
    if(this.authCheck()) {
        return this.sharedPref.iPzmz.ieFname;
    }
    return "customer";
};

UserService.prototype.setUserImage = function(imageUrl){
    if(this.authCheck()){
        this.sharedPref.iPzmz.ieProfilePic = imageUrl;
        this.userImage = imageUrl;
        return true;
    }
    return false;
};

UserService.prototype.getUserImage = function(){
    if(this.authCheck())
        this.userImage = this.sharedPref.iPzmz.ieProfilePic;
    return this.userImage;
};

UserService.prototype.registerComp = function(object){
    var exist = false;
    for(var i=0;i<this.components.length;i++){
        if(Object.is(this.components[i], object)){
            exist = true;
        }
    }
    if(!exist){
        this.components.push(object);
    }
};

UserService.prototype.broadcastEvent = function(event, data){
    var self = this;
    switch(event){
        case 'PICTURE_CHANGE':
            this.components.forEach(function(ele, ind, i){
                if(ele.hasOwnProperty('userImage')){
                    self.userImage = data;
                    ele.userImage = self.userImage;
                }
            });
            break;
    }
};

UserService.prototype.unRegisterComp = function(object){

};