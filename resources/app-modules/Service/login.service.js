/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 09/12/2015.
 */
angular.module('application')
    .service('loginService', ['$http','$localStorage', '$window','errorService','$timeout', LoginService]);

function LoginService($http, $localStorage, $window, errorService, $timeout){
    this.httpServer = $http;
    this.sharedPref = $localStorage;
    this.window = $window;
    this.$errorSvc = errorService;
    this.tOut = $timeout;
}

LoginService.prototype.doLogin = function(data, callback){
    var self = this;
    this.httpServer.post(getPathUrl('/reg/login', false, this.sharedPref),data).then(
        function(response){
            console.log(response);
            var status = response.data.status;
            if(status==="success"){
                self.createSession(response.data.message);
                self.window.location.href = portal;
                callback(true, response.data.message);
            }else{
                console.log(response);
                callback(false, response.data.message.reason);
            }
        },
        function(error){
            console.log(error);
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.doLogin(data, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    if(error.status===401){
                        callback(false, "ERROR_401");
                    }else{
                        callback(false, "ERROR_500");
                    }
                }
            });
        });
};

LoginService.prototype.onRetrievePassword = function(data, callback){
    var self = this;
    this.httpServer.post(getPathUrl('/reg/account/password/reset', false, this.sharedPref),data).then(
        function(response){
            console.log(response);
            var status = response.data.status;
            if(status==="success"){
                callback(true, response.data.message);
            }else{
                console.log(response);
                callback(false, response.data.message.reason);
            }
        },
        function(error){
            console.log(error);
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.onRetrievePassword(data, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    if(error.status===401){
                        callback(false, "ERROR_401");
                    }else{
                        callback(false, "ERROR_500");
                    }
                }
            });
        });
};

LoginService.prototype.doLogout = function(data, callback){
    var self = this;
    this.httpServer.post(getPathUrl('/reg/logout', false, this.sharedPref), data).then(
        function(response){
            console.log(response);
            var status = response.data.status;
            if(status==="success"){
                self.clearSession(response.data.message);
                self.window.location.href = index;
                callback(true, response.data.message);
            }else{
                console.log(response);
                callback(false, response.data.message.reason);
            }
        },
        function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.doLogout(data, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    callback(false);
                }
            });
        });
};

LoginService.prototype.createSession = function(userInfo){
    this.sharedPref.iPzmz = {
        ieBid:userInfo.id,
        ieAno:userInfo.name,
        ieTkup:userInfo.token.usr_tk_token,
        ieTkXEDt:userInfo.token.created_at,
        ieFname:userInfo.cstm_firstname_c + " "+userInfo.cstm_lastname_c,
        ieProfilePic:userInfo.user.pt_user_image
    };
};

LoginService.prototype.clearSession = function(){
    delete this.sharedPref.iPzmz;
};