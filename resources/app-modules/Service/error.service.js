/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * @author Okeke Paul Ugochukwu
 * @email pugochukwu@vas-consulting.com
 * @alt-email donpaul120@gmail.com
 * Created by paulex10 on 12/12/2015.
 */
angular.module('application')
    .factory('errorService',['userService',ErrorService]);

var isError = false;
var retry = false;
var errorMessage = "";
var errorResolution = "";
var userSvc = null;

const RETRY_REQUEST = "RETRY_REQUEST";
const REPLY_FALSE = "REPLY_FALSE";

function ErrorService(userService){
    userSvc = userService;
    return {
        hasError:hasError,
        errorMessage:getErrorMessage,
        errorResolution:getErrorResolution,
        responseError:responseError,
        responseSuccess:responseSuccess,
        hasRetry:isRetry
    }
}

function responseSuccess(response){
    switch (response.status){
        case 200:
            isError = false;
            errorMessage = "";
            break;
    }
}

/**
 * This method should be used to intercept all response error
 * when making a http request.
 * @param response
 * @param callback
 */
function responseError(response, callback){
    console.log(response.status);
    isError = true;
    if(response.hasOwnProperty('status')){
        var respCode = response.status;
        switch (respCode){
            case 0://UNSENT
                //this can be assumed that there isn't an internet connection
                errorMessage = "Hi,  "+userSvc.getUserFullName()+", " +
                    "we are having trouble connecting to the internet, please kindly check your internet connection.";
                errorResolution = "re-trying connection";
                retry = true;
                callback(RETRY_REQUEST);
                break;
            case 500://internal server error
                errorMessage = "Hi, "+userSvc.getUserFullName()+", " +
                    "please something went wrong and we are currently fixing it.";
                errorResolution = "please try again later";
                retry = false;
                callback(REPLY_FALSE);
                //now lets secretly check for error
                break;
            case 401:
                errorMessage = "Your session has expired please login again";
                errorResolution = "redirecting";
                setTimeout(function(){
                    delete userSvc.sharedPref.iPzmz;
                    window.location.href = index;
                },5000);
                retry= false;
                callback(REPLY_FALSE);
                break;
            case 404:
                errorMessage = "The request you made can't be found";
                errorResolution = "Please tell us what you are trying to do";
                retry= false;
                callback(REPLY_FALSE);
                break;
        }
    }
}

function hasError(){
    return isError;
}

function getErrorMessage(){
    return errorMessage;
}

function getErrorResolution(){
    return errorResolution;
}

function isRetry(){
    return retry;
}