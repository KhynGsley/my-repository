/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 09/12/2015.
 */
angular.module('application')
    .controller('LoginController', ['loginService', LoginController]);

function LoginController(loginService){
    this.loginSvc = loginService;
    this.onLogin = false;
    this.isForgotMode = false;
    this.onError = false;
    this.pageState = "Sign In";
    this.loginErrorMsg = "";
    this.infoMessage = "";
    this.loginForm = {
        portal_username:null,
        portal_password:null
    };
    this.resetForm = {
        user_email:null,
        account_number:null
    };
}

LoginController.prototype.login = function(){
    this.onLogin = true;
    this.pageState = "Signing In...";

    this.loginSvc.doLogin(this.loginForm, function(status, $data){
        this.onLogin = false;
        if(status){
            this.pageState = "Sign In";

            //window.location.href = "http://localhost:9010/portal";
        }else{
            this.onError = true;
            switch ($data){
                case "INVALID_LOGIN":
                    this.loginErrorMsg = "Invalid Username or Password";
                    break;
                case "UN_ACTIVATED":
                    this.loginErrorMsg = "User not Activated";
                    break;
                case "ERROR_500":
                    this.loginErrorMsg = "An error occurred";
                    break;
                case "ERROR_401":
                    this.loginErrorMsg = "User doesn't exist";
                    break;
            }
            this.pageState = "Sign In";
        }
    }.bind(this));
};

LoginController.prototype.retrievePassword = function(){
    //1 we need to confirm if we have the email first
    //2 if the email exist then send the email...
    //3 if the email doesn't exist display that the user with the email wasn't found
    this.onLogin = true;
    this.pageState = "Confirming E-mail";
    this.loginSvc.onRetrievePassword(this.resetForm, function(status, $data){
        this.onLogin = false;
        if(status){
            this.pageState = "";
            this.infoMessage = "A link to reset your password has been sent to your email address";
            this.resetForm.account_number = null;
            this.resetForm.user_email = null;
        }else{
            this.onError = true;
            this.pageState = "Retrieve Password";
            switch ($data){
            case "INVALID_USER":
                this.loginErrorMsg = "The User with the email doesn't exist";
                break;
            }
        }
    }.bind(this))
};


LoginController.prototype.initForgotMode = function(state){
    if(state) {
        this.isForgotMode = true;
        this.pageState = "Retrieve Password";
    }else{
        this.isForgotMode = false;
        this.pageState = "Sign In";
        this.infoMessage = "";
    }
};