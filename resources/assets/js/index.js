/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * @author Okeke Paul Ugochukwu
 * @email pugochukwu@vas-consulting.com
 * @alt-email donpaul120@gmail.com
 * Created by paulex10 on 08/12/2015.
 */


"use strict";
$(document).ready(function(){
    indexInit();
});

function indexInit(){
    $('.ie-overlay').hide();
    regIEPortalActions();
    onLoginRegAcct();
    helpEventRegistry();
    validateAllPhoneNumbers();
    validateAllUsername();
}

function regIEPortalActions(){
    var ieRegBtn = document.getElementById('ie-reg-btn');
    var sideNav = document.getElementById('ie-side-name');
    var loginSec = document.getElementsByClassName('ie-login-page').item(0);
    var regSec = document.getElementsByClassName('ie-reg-page').item(0);
    if(ieRegBtn!==null) {
        ieRegBtn.addEventListener('click', function () {
            sideNav.textContent = "Register";
            regSec.classList.add('display');
            regSec.classList.remove('noDisplay');
            loginSec.classList.add('noDisplay');
            vasSlidePage(true);
        });
    }

    var ieLoginBtn = document.getElementById('ie-login-btn');
    if(ieLoginBtn!==null) {
        ieLoginBtn.addEventListener('click', function () {
            sideNav.textContent = "Login";
            regSec.classList.add('noDisplay');
            loginSec.classList.add('display');
            loginSec.classList.remove('noDisplay');
            vasSlidePage(true);
        });
    }

    var backBtn = document.getElementsByClassName('ie-side-back-btn').item(0);
    if(backBtn!==null) {
        backBtn.addEventListener('click', function () {
            //sideNav.textContent = "";
            vasSlidePage(false);
        });
    }
    proceedToLogin();
}

function vasSlidePage(state){
    var portalDes = document.getElementsByClassName('ie-portal-design').item(0);
    var ieOverlay = document.getElementsByClassName('ie-overlay').item(0);
    var footer = document.getElementsByClassName('ie-footer').item(0);
    if(state) {
        portalDes.style.width = 75 + "%";
        ieOverlay.style.width = 75 + "%";
        footer.style.width = 75 + "%";
        $('.ie-portal-actions').slideUp("slow",function(){
            $('.ie-overlay').fadeIn();
        });
    }else{
        portalDes.style.width = 100 + "%";
        ieOverlay.style.width = 100 + "%";
        footer.style.width = 100 + "%";
        $('.ie-overlay').fadeOut();
        $('.ie-portal-actions').slideDown();
    }
}

function proceedToLogin(){
    var pL = document.getElementById('ieLoginProceed');
    if(pL!==null) {
        pL.addEventListener('click', function () {
            vasSlidePage(false);
            setTimeout(function () {
                var ieLoginBtn = document.getElementById('ie-login-btn');
                ieLoginBtn.dispatchEvent(new Event('click'));
            }, 600);
        });
    }
}

function onLoginRegAcct(){
    var ra = document.getElementsByClassName('ie-lg-reg-acct')[0];
    if(ra!==null && ra!==undefined){
        ra.addEventListener('click', function(){
            vasSlidePage(false);
            setTimeout(function () {
                var ieLoginBtn = document.getElementById('ie-reg-btn');
                ieLoginBtn.dispatchEvent(new Event('click'));
            }, 600);
        });
    }
}


function helpEventRegistry(){
    var loginHelp = document.getElementsByClassName('ie-login-help');
    if(loginHelp!==null && loginHelp.length>0){
        loginHelp = loginHelp.item(0);
        loginHelp.addEventListener('click', function(){
            var lhContent = document.getElementsByClassName('ie-login-help-content');
            if(lhContent!==null && lhContent!==undefined){
                lhContent = lhContent.item(0);
                lhContent.style.bottom = 0;
                setTimeout(function(){
                    lhContent.style.display = "block";
                },5);
            }
        });
    }

    var regHelp = document.getElementsByClassName('ie-reg-help');
    if(regHelp!==null && regHelp.length>0){
        regHelp = regHelp.item(0);
        regHelp.addEventListener('click', function(){
            var rhContent = document.getElementsByClassName('ie-reg-help-content');
            if(rhContent!==null && rhContent!==undefined){
                rhContent = rhContent.item(0);
                rhContent.style.bottom = 0;
                setTimeout(function(){
                    rhContent.style.display = "block";
                },5);
            }
        });
    }

    var helpCloser = document.getElementsByClassName('ie-help-closer');
    if(helpCloser!==null && helpCloser!==undefined){
        for(var i=0;i<helpCloser.length;i++) {
            helpCloser[i].addEventListener('click', function () {
                var helpSection = this.parentNode.parentNode;
                helpSection.style.bottom = -40 + "%";
                setTimeout(function(){
                    helpSection.style.display = "block";
                },5);
            });
        }
    }
}

/**
 * @author Okeke Paul
 * Validate all phone numbers by disabling string values
 */
function validateAllPhoneNumbers(){
    var inputs = document.getElementsByTagName('input');
    for(var i=0;i<inputs.length;i++){
        if(inputs[i].type.toLowerCase()==='tel'){
            inputs[i].addEventListener('input', function(){
                var input = this;
                input.value = input.value.replace(/[^0-9]/g, "");
            });
        }
    }
}

function validateAllUsername(){
    var inputs = document.getElementsByTagName('input');
    for(var i=0;i<inputs.length;i++){
        if(inputs[i].name.toLowerCase()==='username'){
            inputs[i].addEventListener('input', function(){
                var input = this;
                input.value = input.value.replace(/[^0-9^A-Za-z]/g, "");
            });
        }
    }
}