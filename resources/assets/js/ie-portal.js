/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */
/**
 * @author Paul Okeke
 * @company VAS-CONSULTING
 * Created by paulex10 on 21/11/2015.
 */

"use strict";

var currentPosition = 0+'px';
$(document).ready(function(){
    //initializeCaseTable();
    registerNavClickEvent();
    regProfilePicFileEvent();
});

function registerNavClickEvent(){
    var nav = document.getElementsByClassName("nav-item");

    for(var i=0; i<nav.length; i++){

        nav[i].addEventListener('click', function(){
            var mainScroll = document.getElementsByClassName('ie-scroll').item(0);
            var sectionId = this.getAttribute('data-section');
            //lets make this navigation the selected anchor
            var currentNav = document.querySelector('.ie-nav-active');
            if(currentNav!==undefined && currentNav!==null){
                currentNav.classList.remove('ie-nav-active');
                //currentNav.firstElementChild.classList.remove('ie-anchor-active');
            }
            this.parentNode.classList.add('ie-nav-active');
            //this.classList.add('ie-anchor-active');
            if(sectionId!==undefined && sectionId!==''){
                var sectionPage = document.getElementById(sectionId);
                if(sectionPage!==null) {
                    var flipPageTo = sectionPage.style.left;
                    mainScroll.style.transform = "translate3d(-" + flipPageTo + ", 0, 0)";
                }
            }
        });
    }
    regNavSubItemEvent();
}

function regNavSubItemEvent(){
    var navSubItem = document.getElementsByClassName('nav-sub-item').item(0);
    var subNavCont = document.getElementsByClassName('ie-sub-nav-cont').item(0);
    if(navSubItem!==null && navSubItem!==undefined) {
        navSubItem.addEventListener('click', function () {
            subNavCont.style.top = 88+"px";
        });

        var html = document.getElementsByTagName('html').item(0);
        if(html!==null) {
            html.addEventListener('click', function (event) {
                var top = subNavCont.style.top;
                if(event.target.offsetParent!==null) {
                    var cont = event.target.offsetParent.className;
                }
                var src = event.srcElement.className;
                if (cont !== 'ie-sub-nav-cont' && src !== 'nav-sub-item' && top === "88px") {
                    subNavCont.style.top = -88 + "px";
                }
            });
        }
    }
}

function onCaseViewEvent(){
    var cNos = document.getElementsByClassName('ie-case-no');
    var event = new Event('click');
    for(var i=0;i<cNos.length;i++){
        cNos[i].addEventListener('click', function(){

            var caseMajorView = document.getElementsByClassName('ie-page-content-sub').item(0);
            caseMajorView.style.display = "block";
            setTimeout(function(){
                caseMajorView.style.top = 0+'%';
            },5);
            setTimeout(function(){
                var mainCaseListView = document.getElementById('case-parent');
                mainCaseListView.style.display =  "none";
            },1200);
            //we need to be able to get all the details
            var chosenRecord = JSON.parse(this.getAttribute("data-case-id"));
            createDetailView(chosenRecord);
            var angularSpecial = document.getElementById('ang-special');
            //set the case-id here
            angularSpecial.setAttribute('data-case-id', chosenRecord.id.value);
            angularSpecial.dispatchEvent(event);
        });
    }
}

function closeDetailedView(){
    var closeDiv = document.getElementsByClassName('ie-down-arrow').item(0);
    if(closeDiv!==null && closeDiv!==undefined) {
        closeDiv.addEventListener('click', function () {
            var angularSpecial = document.getElementById('ang-special');
            angularSpecial.dispatchEvent(new Event('click'));
            var mainCaseListView = document.getElementById('case-parent');
            mainCaseListView.style.display =  "block";
            var caseMajorView = document.getElementsByClassName('ie-page-content-sub').item(0);
            caseMajorView.style.top = 100 + '%';
            setTimeout(function(){
                var subView = document.getElementsByClassName('ie-page-content-sub').item(0);
                subView.style.display =  "none";
                resetNoteFileInput();
            },1200);
        });
    }
}

function createDetailView($data){
    var dCase = document.getElementById('d-case-no');
    if($data.hasOwnProperty('case_number'))
    dCase.textContent = "#"+$data['case_number'].value;

    if($data.hasOwnProperty('status')) {
        var dStatus = document.getElementsByClassName('ie-detailed-case-status').item(0);
        dStatus.textContent = $data['status'].value.toString().toUpperCase();
        getCaseStatusClass(dStatus, $data['status'].value.toString());
    }

    if($data.hasOwnProperty('name')) {
        var dSubject = document.getElementsByClassName('ie-case-detailed-subject').item(0);
        dSubject.textContent = $data['name'].value;
    }

    if($data.hasOwnProperty('cstm_complaint_category_c')) {
        var dMajCat = document.getElementById('ie-maj-cat-val');
        dMajCat.textContent = $data['cstm_complaint_category_c'].value;
    }

    if($data.hasOwnProperty('cstm_complaint_sub_category_c')) {
        var dSubCat = document.getElementById('ie-sub-cat-val');
        dSubCat.textContent = $data['cstm_complaint_sub_category_c'].value;
    }

    if($data.hasOwnProperty('priority')) {
        var dPriority = document.getElementById('ie-priority-val');
        dPriority.textContent = $data['priority'].value;
    }

    if($data.hasOwnProperty('description')) {
        var dDesc = document.getElementsByClassName('ie-case-detailed-desc')[0];
        dDesc.textContent = $data['description'].value;
    }

    if($data.hasOwnProperty('cstm_resolution_summary_c')) {
        var dResolution = document.getElementsByClassName('ie-case-detailed-resolution')[0];
        dResolution.textContent = $data['cstm_resolution_summary_c'].value;
    }
}

function getCaseStatusClass(element, status){
    status = status.toLowerCase();
    var obviousStatus = document.getElementsByClassName('ie-case-detailed-cont-bg');
    switch(status){
        case 'new':
            element.style.background = "#01579B";
            obviousStatus[0].style.borderTop = "1px solid #01579B";
            obviousStatus[1].style.borderTop = "1px solid #01579B";
            break;
        case 'open':
            element.style.background = "#00E676";
            obviousStatus[0].style.borderTop = "1px solid #00E676";
            obviousStatus[1].style.borderTop = "1px solid #00E676";
            break;
        case 'closed':
            element.style.background = "#D32F2F";
            obviousStatus[0].style.borderTop = "2px solid #40C4FF";
            obviousStatus[1].style.borderTop = "2px solid #40C4FF";
            break;
        case 'pending':
            element.style.background = "#FFD600";
            obviousStatus[0].style.borderTop = "1px solid #FFD600";
            obviousStatus[1].style.borderTop = "1px solid #FFD600";
            break;
        case 'in_progress':
            element.style.background = "#FF9100";
            obviousStatus[0].style.borderTop = "1px solid #FF9100";
            obviousStatus[1].style.borderTop = "1px solid #FF9100";
            break;
    }
}

function onAttachFile(){
    var image = document.getElementById('ie-file-trigger');
    var fileInput = document.getElementById('ie-file-attach');
    image.addEventListener('click', function(){

        fileInput.dispatchEvent(new Event('click'));
    });
    validateFileUploads('3mb', [
        'image/png', 'image/jpg',
        'image/jpeg', 'image/gif','application/pdf',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ],'ie-file-attach', showFileName);
}

function showFileName(fileInput){
        var fnNode = document.getElementsByClassName('ie-file-name')[0];
        fnNode.textContent = fileInput.files[0].name;
}

function resetNoteFileInput(){
    var fileInput = document.getElementById('ie-file-attach');
    document.getElementsByClassName('ie-file-name')[0].textContent ="";

    //fileInput.parentNode.replaceChild(fileInput.cloneNode(true), fileInput);
}

//function getCasePriority(priority){
//    priority = priority.toLowerCase();
//
//    switch (priority){
//        case "p1":
//            return "High";
//            break;
//        case "p2":
//            return "Medium";
//            break;
//        case "p3":
//            return "Low";
//            break;
//        default :
//            return "Low";
//            break;
//    }
//}

function getStatusForTable($status){
    $status = $status.toLowerCase();
    switch($status){
        case 'new':
            return '<span class="case-new ie-case-status">'+$status.toUpperCase()+'</span>';
            break;
        case 'open':
            return '<span class="case-open ie-case-status">'+$status.toUpperCase()+'</span>';
            break;
        case 'closed':
            return '<span class="case-closed ie-case-status">'+$status.toUpperCase()+'</span>';
            break;
        case 'pending':
            return '<span class="case-pending ie-case-status">'+$status.toUpperCase()+'</span>';
            break;
        case 'in_progress':
            return '<span class="case-progress ie-case-status">'+$status.toUpperCase()+'</span>';
            break;
    }
    return '<span class="ie-case-status">'+$status.toUpperCase()+'</span>';
}

function initializeCaseTable(userID, getPathUrl){
    //$(document).ready(function() {
    if(typeof getPathUrl !== 'function'){
        throw new TypeError('Parameter 2 must be a function');
    }
        var table = $('.ie-case-table').DataTable({
            "oLanguage":{
                "sSearch":" ",
                "sSearchPlaceholder": "search"
            },
            "initComplete": function(settings, json) {
                table.order([['6','desc'],['0','desc']]).draw();
                onCaseViewEvent();
            },
            "lengthMenu":[6,8],
            "processing": false,
            "serverSide": false,
            "ajax":{
                url:getPathUrl('/contact/cases/'+userID.getUserId(), true, userID.sharedPref),
                beforeSend: function (request) {
                    request.setRequestHeader("vas-token", JSON.parse(
                        window.localStorage.getItem('ngStorage-iPzmz')).ieTkup);
                }
            }
        });
        //lets order the table to show the recent case...

        $('.ie-case-table tbody').on('click','tr td .ie-case-no', function(){
            onCaseViewEvent();
            $(this)[0].dispatchEvent(new Event('click'));
        });
        //$('.ie-case-table').on('page', onCaseViewEvent);
    //} );
}

/**
 * We are going to use this function
 * to monitor screen changes and all that reponsive stuffs
 */
function setSectionPosition(){
    var mql300 = window.matchMedia("(min-width:300px)");
    mql300.addListener(handle300Width);
    var mql640 = window.matchMedia("(min-width:640px)");
    mql640.addListener(handle640Width);
    var mql1280 = window.matchMedia("(min-width:1280px)");
    mql1280.addListener(handle1280Width);
    handle300Width(mql300);
    handle640Width(mql640);
    handle1280Width(mql1280);
}

function handle300Width(mql){
    if(mql.matches){
        adjustScreen();
        //console.log("there is a match");
    }else{
        //console.log("no match 300");
    }
}

function handle640Width(mql){
    if(mql.matches){
        adjustScreen();
        //console.log("there is a match");
    }else{
        //console.log("no match 300");
    }
}function handle1280Width(mql){
    if(mql.matches){
        adjustScreen();
        //console.log("there is a match");
    }else{
        console.log("no match 300");
    }
}

function adjustScreen(){
    var screenWidth = window.innerWidth;

    var casesSection = document.getElementById('cases');
    if(casesSection!==null) {
        casesSection.style.left = screenWidth + "px";
    }

    var faqSection = document.getElementById('faqs');
    if(faqSection!==null) {
        faqSection.style.left = screenWidth * 2 + "px";
    }

    var profileSection = document.getElementById('profile');
    if(profileSection!==null) {
        profileSection.style.left = screenWidth * 3 + "px";
    }
}

function regProfilePicFileEvent(){
    validateFileUploads('1mb',
        ['image/png', 'image/jpeg', 'image/jpg'],
        'ie-customer-pic',
        drawImage
    );
    var fileInput = document.getElementsByName('ie-customer-pic').item(0);
    var brwBtn = document.getElementsByClassName('ie-browse-btn').item(0);
    if(brwBtn!==null){
        brwBtn.addEventListener('click', function(){
            if(fileInput!==null){
                fileInput.dispatchEvent(new Event('click'));
            }
        });
    }
}

function drawImage(fileInput){
    var fileReader = new FileReader();
    var imgDisplay = document.getElementById('ie-user-displayer');
    if(fileInput!==null) {
        if (fileInput.files.length > 0) {
            document.getElementById('ie-fileup-name').textContent = fileInput.files[0].name;
            fileReader.onloadend = function (e) {
                imgDisplay.src = e.target.result;
            };
            fileReader.readAsDataURL(fileInput.files[0]);
        }
    }
}