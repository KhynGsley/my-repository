/**
 * Author Paul Okeke && Kingsley Otugo
 * Since 2014
 * Company VAS-CONSULTING LTD
 *
 * This document is a solution towards
 * three Dependent DropDown and two Dependent
 *
 * 
 * Updated on 25/11/2015
**/

//Lets first understand what the problem is:
//We should have two drop downs
//DropDown A and DropDown B
//DropDown B is dependent on DropDown A

var father_cat_field = "majorCategory";
var mother_cat_field = "subCategory";
var child_cat_field = "cstm_job_sub_type_c";
//specify if the dropdown type, is it two(2) or three(3) dependent
//for now the maximum is three
var dropDownType = 2;

var fatherData = {
    "technical": [
        {
            "faultytransformer": "Complaint on transformer"
        },
        {
            "fluctuatingpowersupply": "Voltage Related Complaint"
        },
        {
            "nopowersupply": "No Power Supply"
        },
        {
            "wirecut": "Wire Cut"
        },
        {
            "brokenpole": "Broken Pole"
        }
    ],
    "commecial": [
        {
            "billclarification": "Bill Complaints"
        },
        {
            "stopbilling": "Bill Termination"
        },
        {
            "changeoftarrif": "Tariff"
        },
        {
            "wrongbillinginfo": "Wrong Billing Information"
        },
        {
            "statementofaccount": "Statement of Account"
        }
    ],
    "ppm": [
        {
            "damagedmeter": "Complaint on meter"
        },
        {
            "disconnectionerror":"Meter Error"
        },
        {
            "lossofsmartcard": "Complaint on meter card"
        },
        {
            "unabletorechargeppm": "Inability to load meter"
        },
        {
            "changeofownership": "Change of meter details"
        },
        {
            "reschedulingofarreas": "Rescheduling of arreas"
        },
        {
            "reqforppm": "Request for PPM"
        }
    ],
    "payment": [
        {
            "invalidcard": "Card related complaints"
        },
        {
            "paymentplatformdowntime": "Unable to make payments"
        },
        {
            "uncreditedpayment2": "Uncredited Payment"
        },
        {
            "mismatchofcustomerdetails": "Wrong details on receipt"
        },
        {
            "paymentintowrongaccount":"Complaint on electric account"
        }
    ],
    "ami": [
        {
            "faulty_smart_meter": "Meter complaints"
        },
        {
            "unabletoloadtoken": "Unable to recharge meter"
        },
        {
            "faulty_uiu": "User Interface Unit (UIU)"
        },
        {
            "no_power_supply": "No power supply"
        },
        {
            "meter_bypass": "Meter Bypass"
        }
    ],
    "requestenquiry": [
        {
            "aboutie": "General Enquiries"
        },
        {
            "enquiryonnewpayment":"Enquiries on payment"
        },
        {
            "complimentarycalls":"Complimentary"
        }
    ]
};
var motherData = {};


$(document).ready(function() {
   /// init();
});

function init()
{
	//Trigger the change event of the major category when the page loads
	//checkDocument();
	setTimeout(function(){
		fatherDropDownEventListener();
		motherDropDownEventListener();
		triggerParentChange(father_cat_field);
	}, 1000);

}


function triggerParentChange(selectField)
{
    var event =  new Event("change");
    var majorCatSelect = document.getElementsByName(selectField).item(0);
	//This will trigger the Event
    if(majorCatSelect!==null)
        majorCatSelect.dispatchEvent(event);
}


function fatherDropDownEventListener()
{
    var majorCatSelect = document.getElementsByName(father_cat_field).item(0);
    if(majorCatSelect!==null ) {
        majorCatSelect.addEventListener("change", function () {
            //lets get the value selected so we can know what
            var selectedValue = majorCatSelect.options[majorCatSelect.selectedIndex].value;
            loadSubDropDown(selectedValue, mother_cat_field, fatherData);
            //tell mama say we don get food for store
            triggerParentChange(mother_cat_field);
        });
    }
}

function motherDropDownEventListener(){
	var majorCatSelect = document.getElementsByName(mother_cat_field).item(0);
	if(dropDownType==3 && majorCatSelect!==null){
		majorCatSelect.addEventListener("change", function(){
			//lets get the value selected so we can know what
			if(majorCatSelect.options.length>0){
				var selectedValue = majorCatSelect.options[majorCatSelect.selectedIndex].value;
				loadSubDropDown(selectedValue, child_cat_field, motherData);
			}else{
				loadSubDropDown(0, child_cat_field, motherData);
			}
		});
	}
}



/**
* @author Paul Okeke
* This function will load the sub-category of the parent value
* @dData is the dependent Data
*/
function loadSubDropDown(parentValue, dSubDropDown, dData)
{	
	var subCategory = document.getElementsByName(dSubDropDown);
	//lets check that the element exists on the page...
	if(subCategory!==null && subCategory!==undefined){
        subCategory = subCategory.item(0);
		//gets the initial value of the sub-category
		var value = getInitialValue(subCategory);
		//empty all the options so as to load it afresh
		subCategory.options.length = 0;
		if(dData.hasOwnProperty(parentValue)) {
			//get the object of the parent value within the json object
			var opt = dData[parentValue];
			for (var i = 0; i < opt.length; i++) {
				for(var key in opt[i])
				{
					var option = document.createElement("option");
					option.value = key;
					//if the key is the same thing as the initial value
					//Then it means this option needs to be selected for editing purpose
					if(key ===value)
					{
						option.selected =true;
					}
					option.text = opt[i][key];
					subCategory.add(option);
				}
			}
		}
	}
}


/**
 * @author Paul Okeke
 * This function is going to get the initial value of the
 * DropDown so when the user wants to edit a particular Case.
 * We can immediately get the initial value
 */
function getInitialValue(selectElement)
{	if(selectElement.options.length>0){
  return selectElement.options[selectElement.selectedIndex].value;
	}else{
	return false;
	}
}